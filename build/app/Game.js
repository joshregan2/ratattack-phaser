var GameScene,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

GameScene = (function() {
  function GameScene(game) {
    this.squishRat = __bind(this.squishRat, this);
    this.handleTap = __bind(this.handleTap, this);
    this.checkIfTapCollidesWithRatSprite = __bind(this.checkIfTapCollidesWithRatSprite, this);
    this.handleRatToHouseCollision = __bind(this.handleRatToHouseCollision, this);
    this.spawnRat = __bind(this.spawnRat, this);
    this.update = __bind(this.update, this);
    this.start = __bind(this.start, this);
    this.game = game;
    this.background = null;
    this.allRats = [];
    this.ratSpawnCounter = 0;
    this.house = null;
    this.gamehud = null;
  }

  GameScene.prototype.start = function() {
    this.background = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'background');
    this.background.anchor.setTo(0.5, 0.5);
    this.house = new House(this.game, this.game.world.centerX, this.game.world.height - 48);
    this.gamehud = new GameHUD(this.game, this.game.world.centerX, this.game.world.height - 16);
    return this.game.input.onDown.add(this.handleTap);
  };

  GameScene.prototype.update = function() {
    var idx, _results;
    if (this.ratSpawnCounter < SharedGameConfig.RAT_SPAWN_TIME) {
      this.ratSpawnCounter++;
    } else {
      this.ratSpawnCounter = 0;
      this.spawnRat();
    }
    idx = 0;
    _results = [];
    while (idx < this.allRats.length) {
      _results.push((function(_this) {
        return function() {
          _this.game.physics.arcade.collide(_this.house, _this.allRats[idx].ratSprite, _this.handleRatToHouseCollision);
          return idx++;
        };
      })(this)());
    }
    return _results;
  };

  GameScene.prototype.spawnRat = function() {
    var max, min, mutantRat, randomRatType, randomX, ratTypes;
    min = 32;
    max = this.game.world.width - 32;
    randomX = Math.floor(Math.random() * (max - min + 1)) + min;
    ratTypes = _.keys(SharedGameConfig.RAT_TYPES);
    randomRatType = ratTypes[Math.floor(Math.random() * ratTypes.length)];
    mutantRat = new MutantRat(this.game, randomX, 64, randomRatType);
    return this.allRats.push(mutantRat);
  };

  GameScene.prototype.handleRatToHouseCollision = function(ratSprite, house) {
    var ratIndex;
    ratIndex = this.allRats.indexOf(ratSprite.parent);
    if (ratIndex !== -1) {
      this.allRats.splice(ratIndex, 1);
    }
    ratSprite.parent.destroy(true);
    house.tint = 0xe73f3f;
    return this.game.time.events.add(Phaser.Timer.SECOND * 0.2, function() {
      return house.tint = 0xffffff;
    }, this);
  };

  GameScene.prototype.checkIfTapCollidesWithRatSprite = function(tapX, tapY, ratSprite) {
    if (tapX >= ratSprite.x - ratSprite.body.halfWidth && tapX <= ratSprite.x + ratSprite.body.halfWidth && tapY >= ratSprite.y - ratSprite.body.halfHeight && tapY <= ratSprite.y + ratSprite.body.halfHeight) {
      return true;
    } else {
      return false;
    }
  };

  GameScene.prototype.handleTap = function() {
    var idx, _results;
    idx = 0;
    _results = [];
    while (idx < this.allRats.length) {
      _results.push((function(_this) {
        return function() {
          if (_this.checkIfTapCollidesWithRatSprite(_this.game.input.worldX, _this.game.input.worldY, _this.allRats[idx].ratSprite) === true) {
            _this.squishRat(_this.allRats[idx]);
          }
          return idx++;
        };
      })(this)());
    }
    return _results;
  };

  GameScene.prototype.squishRat = function(rat) {
    var emitter, ratIndex;
    emitter = this.game.add.emitter(rat.ratSprite.x, rat.ratSprite.y, 50);
    emitter.gravity = 0;
    emitter.makeParticles('rat-bit');
    emitter.start(true, 3000, null, 20);
    ratIndex = this.allRats.indexOf(rat);
    if (ratIndex !== -1) {
      this.allRats.splice(ratIndex, 1);
    }
    rat.destroy(true);
    return this.game.time.events.add(Phaser.Timer.SECOND * 2, this.destroyEmitter, this, emitter);
  };

  GameScene.prototype.destroyEmitter = function(emitter) {
    emitter.destroy(true);
    return emitter = null;
  };

  return GameScene;

})();
;var GameManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

GameManager = (function() {
  function GameManager() {
    this.render = __bind(this.render, this);
    this.update = __bind(this.update, this);
    this.start = __bind(this.start, this);
    this.preload = __bind(this.preload, this);
    this.setup = __bind(this.setup, this);
    this.game = null;
    this.currentScene = null;
  }

  GameManager.prototype.setup = function(frameWidth, frameHeight) {
    return this.game = new Phaser.Game(frameWidth, frameHeight, Phaser.AUTO, '', {
      preload: this.preload,
      create: this.start,
      update: this.update,
      render: this.render
    });
  };

  GameManager.prototype.preload = function() {
    this.game.load.image('background', '/img/background.png');
    this.game.load.image('hud-bar', '/img/hud-bar.png');
    this.game.load.image('integrity-bar', '/img/integrity-bar.png');
    this.game.load.image('security-door', '/img/security-door.png');
    this.game.load.spritesheet('normal-rat', '/img/normal-rat-spritesheet24x48.png', 24, 48, 8);
    this.game.load.image('snake_rat', '/img/snake_rat.png');
    this.game.load.image('house', '/img/house-temp.png');
    return this.game.load.image('rat-bit', '/img/rat-bit.png');
  };

  GameManager.prototype.start = function() {
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    this.currentScene = new GameScene(this.game);
    return this.currentScene.start();
  };

  GameManager.prototype.update = function() {
    if (this.currentScene != null) {
      return this.currentScene.update();
    }
  };

  GameManager.prototype.render = function() {};

  return GameManager;

})();
;window.SharedGameManager = new GameManager();

SharedGameManager.setup(320, 568);
;var GameHUD,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GameHUD = (function(_super) {
  __extends(GameHUD, _super);

  function GameHUD(game, x, y) {
    this.update = __bind(this.update, this);
    var integrityTextStyle, scoreTitleTextStyle, scoreValueTextStyle;
    Phaser.Group.call(this, game);
    this.game = game;
    this.hudBarSprite = new Phaser.Sprite(this.game, x, y, "hud-bar");
    this.hudBarSprite.anchor.setTo(0.5, 0.5);
    this.integrityBarSprite = new Phaser.Sprite(this.game, 3, y - 11, "integrity-bar");
    integrityTextStyle = {
      font: "17px pixelsplitterregular",
      fill: "#ffffff",
      align: "center"
    };
    this.integrityText = this.game.add.text(x - 78, y + 1, "INTEGRITY", integrityTextStyle);
    this.integrityText.anchor.setTo(0.5, 0.5);
    this.weaponText = this.game.add.text(x + 24, y + 1, "WEP", integrityTextStyle);
    this.weaponText.anchor.setTo(0.5, 0.5);
    scoreTitleTextStyle = {
      font: "7px pixelsplitterregular",
      fill: "#ffffff",
      align: "center"
    };
    this.scoreTitleText = this.game.add.text(x + 118, y - 6, "SCORE", scoreTitleTextStyle);
    this.scoreTitleText.anchor.setTo(0.5, 0.5);
    scoreValueTextStyle = {
      font: "15px pixelsplitterregular",
      fill: "#ffffff",
      align: "center"
    };
    this.scoreValueText = this.game.add.text(x + 118, y + 5, "0", scoreValueTextStyle);
    this.scoreValueText.anchor.setTo(0.5, 0.5);
    this.add(this.hudBarSprite);
    this.add(this.integrityBarSprite);
    this.add(this.integrityText);
    this.add(this.weaponText);
    this.add(this.scoreTitleText);
    this.add(this.scoreValueText);
  }

  GameHUD.prototype.update = function() {
    return GameHUD.__super__.update.call(this);
  };

  return GameHUD;

})(Phaser.Group);
;var House,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

House = (function(_super) {
  __extends(House, _super);

  function House(game, x, y) {
    this.update = __bind(this.update, this);
    Phaser.Group.call(this, game);
    this.game = game;
    this.houseSprite = new Phaser.Sprite(this.game, x, y, "security-door");
    this.houseSprite.anchor.setTo(0.5, 0.5);
    this.add(this.houseSprite);
    this.game.physics.enable(this.houseSprite, Phaser.Physics.ARCADE);
    this.houseSprite.body.immovable = true;
    this.houseSprite.body.allowGravity = false;
  }

  House.prototype.update = function() {
    return House.__super__.update.call(this);
  };

  return House;

})(Phaser.Group);
;var MutantRat,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

MutantRat = (function(_super) {
  __extends(MutantRat, _super);

  function MutantRat(game, x, y, ratType) {
    this.update = __bind(this.update, this);
    this.getImageForRatType = __bind(this.getImageForRatType, this);
    Phaser.Group.call(this, game);
    this.game = game;
    this.ratType = ratType;
    this.ratSprite = new Phaser.Sprite(this.game, x, y, this.getImageForRatType(ratType));
    this.ratSprite.animations.add('run');
    this.ratSprite.animations.play('run', 20, true);
    this.ratSprite.anchor.setTo(0.5, 0.5);
    this.add(this.ratSprite);
    this.game.physics.enable(this.ratSprite, Phaser.Physics.ARCADE);
    this.ratSprite.body.collideWorldBounds = true;
    this.ratSprite.body.velocity.y = SharedGameConfig.RAT_MOVEMENT_SPEEDS[this.ratType];
  }

  MutantRat.prototype.getImageForRatType = function(ratType) {
    switch (ratType) {
      case SharedGameConfig.RAT_TYPES.NORMAL:
        return "normal-rat";
      case SharedGameConfig.RAT_TYPES.SNAKE:
        return "normal-rat";
      default:
        return "";
    }
  };

  MutantRat.prototype.update = function() {
    return MutantRat.__super__.update.call(this);
  };

  return MutantRat;

})(Phaser.Group);
;var GameConfig;

GameConfig = (function() {
  function GameConfig() {
    this.RAT_TYPES = {
      NORMAL: "NORMAL",
      SNAKE: "SNAKE"
    };
    this.RAT_MOVEMENT_SPEEDS = {
      NORMAL: 200,
      SNAKE: 150
    };
    this.RAT_SPAWN_TIME = 40;
  }

  return GameConfig;

})();

window.SharedGameConfig = new GameConfig();
;
//# sourceMappingURL=Game.js.map
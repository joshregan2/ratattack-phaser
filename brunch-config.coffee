exports.config =
  # See http://brunch.io/#documentation for docs.

  paths:
    public: './build'
    watched: [
      'app',
      'assets',
      'style',
      'vendor',
    ]

  modules:
    definition: false
    wrapper: false

  files:
    javascripts:
      joinTo:
        'app/Game.js': /^app/
        'app/vendor.js': /^vendor/
      order:
        before: [
          'app/components/GameScene.coffee'
          'app/managers/GameManager.coffee'
        ]
    stylesheets:
      joinTo: 'app.css'
    templates:
      joinTo: 'app.js'

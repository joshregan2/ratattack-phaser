class House extends Phaser.Group
  constructor: (game, x, y) ->
    Phaser.Group.call(this, game);
    @game = game

    @houseSprite = new Phaser.Sprite(@game, x, y, "security-door")
    @houseSprite.anchor.setTo(0.5, 0.5)

    @add(@houseSprite)

    @game.physics.enable(@houseSprite, Phaser.Physics.ARCADE)

    @houseSprite.body.immovable = true
    @houseSprite.body.allowGravity = false

  update: () =>
    super()

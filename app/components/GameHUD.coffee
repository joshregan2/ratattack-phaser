class GameHUD extends Phaser.Group
  constructor: (game, x, y) ->
    Phaser.Group.call(this, game);
    @game = game

    @hudBarSprite = new Phaser.Sprite(@game, x, y, "hud-bar")
    @hudBarSprite.anchor.setTo(0.5, 0.5)

    @integrityBarSprite = new Phaser.Sprite(@game, 3, y-11, "integrity-bar")

    integrityTextStyle = {
      font: "17px pixelsplitterregular"
      fill: "#ffffff"
      align: "center"
    }
    @integrityText = @game.add.text(
      x-78,
      y+1,
      "INTEGRITY",
      integrityTextStyle
    );
    @integrityText.anchor.setTo(0.5, 0.5)

    @weaponText = @game.add.text(
      x+24,
      y+1,
      "WEP",
      integrityTextStyle
    );
    @weaponText.anchor.setTo(0.5, 0.5)

    scoreTitleTextStyle = {
      font: "7px pixelsplitterregular"
      fill: "#ffffff"
      align: "center"
    }
    @scoreTitleText = @game.add.text(
      x+118,
      y-6,
      "SCORE",
      scoreTitleTextStyle
    );
    @scoreTitleText.anchor.setTo(0.5, 0.5)

    scoreValueTextStyle = {
      font: "15px pixelsplitterregular"
      fill: "#ffffff"
      align: "center"
    }
    @scoreValueText = @game.add.text(
      x+118,
      y+5,
      "0",
      scoreValueTextStyle
    );
    @scoreValueText.anchor.setTo(0.5, 0.5)

    @add(@hudBarSprite)
    @add(@integrityBarSprite)
    @add(@integrityText)
    @add(@weaponText)
    @add(@scoreTitleText)
    @add(@scoreValueText)

  update: () =>
    super()

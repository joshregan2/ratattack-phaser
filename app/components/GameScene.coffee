class GameScene
  constructor:(game) ->
    @game = game
    @background = null

    @allRats = []
    @ratSpawnCounter = 0

    @house = null

    @gamehud = null

  start:() =>
    @background = @game.add.sprite(@game.world.centerX, @game.world.centerY, 'background');
    @background.anchor.setTo(0.5, 0.5);

    @house = new House(@game, @game.world.centerX, @game.world.height-48)

    @gamehud = new GameHUD(@game, @game.world.centerX, @game.world.height-16)

    @game.input.onDown.add(@handleTap)

  update: () =>
    if @ratSpawnCounter < SharedGameConfig.RAT_SPAWN_TIME
      @ratSpawnCounter++
    else
      @ratSpawnCounter = 0
      @spawnRat()

    idx = 0;
    while idx < @allRats.length
      do () =>
        @game.physics.arcade.collide(@house, @allRats[idx].ratSprite, @handleRatToHouseCollision)
        idx++

  spawnRat: () =>
    min = 32
    max = @game.world.width - 32
    randomX = Math.floor(Math.random() * (max - min + 1)) + min;

    ratTypes = _.keys(SharedGameConfig.RAT_TYPES)
    randomRatType = ratTypes[Math.floor(Math.random() * (ratTypes.length))]

    mutantRat = new MutantRat(@game, randomX, 64, randomRatType)
    @allRats.push(mutantRat)

  handleRatToHouseCollision: (ratSprite, house) =>
    ratIndex = @allRats.indexOf(ratSprite.parent)
    if ratIndex isnt -1
      @allRats.splice(ratIndex, 1)

    ratSprite.parent.destroy(true)
    house.tint = 0xe73f3f
    @game.time.events.add(Phaser.Timer.SECOND*0.2, () ->
        house.tint = 0xffffff
    , this);

  checkIfTapCollidesWithRatSprite: (tapX, tapY, ratSprite) =>
    if tapX >= ratSprite.x - ratSprite.body.halfWidth and tapX <= ratSprite.x + ratSprite.body.halfWidth and
    tapY >= ratSprite.y - ratSprite.body.halfHeight and tapY <= ratSprite.y + ratSprite.body.halfHeight
      return true
    else
      return false

  handleTap: () =>
    idx = 0
    while idx < @allRats.length
      do () =>
        if @checkIfTapCollidesWithRatSprite(@game.input.worldX, @game.input.worldY, @allRats[idx].ratSprite) is true
          @squishRat(@allRats[idx])
        idx++

  squishRat: (rat) =>
    emitter = @game.add.emitter(rat.ratSprite.x, rat.ratSprite.y, 50);
    emitter.gravity = 0;
    emitter.makeParticles('rat-bit');
    emitter.start(true, 3000, null, 20);

    ratIndex = @allRats.indexOf(rat)
    if ratIndex isnt -1
      @allRats.splice(ratIndex, 1)
    rat.destroy(true)

    @game.time.events.add(Phaser.Timer.SECOND * 2, @destroyEmitter, this, emitter);

  destroyEmitter: (emitter) ->
    emitter.destroy(true)
    emitter = null

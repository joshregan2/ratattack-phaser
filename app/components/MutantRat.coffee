class MutantRat extends Phaser.Group
  constructor: (game, x, y, ratType) ->
    Phaser.Group.call(this, game);
    @game = game
    @ratType = ratType

    @ratSprite = new Phaser.Sprite(@game, x, y, @getImageForRatType(ratType))
    @ratSprite.animations.add('run');
    @ratSprite.animations.play('run', 20, true);

    @ratSprite.anchor.setTo(0.5, 0.5)

    @add(@ratSprite)

    @game.physics.enable(@ratSprite, Phaser.Physics.ARCADE)
    @ratSprite.body.collideWorldBounds = true

    @ratSprite.body.velocity.y = SharedGameConfig.RAT_MOVEMENT_SPEEDS[@ratType]


  getImageForRatType: (ratType) =>
    switch ratType
      when SharedGameConfig.RAT_TYPES.NORMAL then return "normal-rat"
      when SharedGameConfig.RAT_TYPES.SNAKE then return "normal-rat"

      else return ""

  update: () =>
    super()

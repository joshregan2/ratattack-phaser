class GameConfig
  constructor: () ->
    @RAT_TYPES = {
        NORMAL: "NORMAL"
        SNAKE: "SNAKE"
    }

    @RAT_MOVEMENT_SPEEDS = {
        NORMAL: 200
        SNAKE: 150
    }

    @RAT_SPAWN_TIME = 40

window.SharedGameConfig = new GameConfig()

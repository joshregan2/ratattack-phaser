class GameManager
  constructor: () ->
    @game = null
    @currentScene = null

  setup: (frameWidth, frameHeight) =>
    @game = new Phaser.Game(frameWidth, frameHeight, Phaser.AUTO, '', { preload: @preload, create: @start, update: @update, render: @render })

  preload: () =>
    @game.load.image('background', '/img/background.png')
    @game.load.image('hud-bar', '/img/hud-bar.png')
    @game.load.image('integrity-bar', '/img/integrity-bar.png')
    @game.load.image('security-door', '/img/security-door.png')
    @game.load.spritesheet('normal-rat', '/img/normal-rat-spritesheet24x48.png', 24, 48, 8);
    @game.load.image('snake_rat', '/img/snake_rat.png')
    @game.load.image('house', '/img/house-temp.png')
    @game.load.image('rat-bit', '/img/rat-bit.png')

  start: () =>
    @game.physics.startSystem(Phaser.Physics.ARCADE);
    @currentScene = new GameScene(@game)
    @currentScene.start()

  update: () =>
    if @currentScene?
      @currentScene.update()

  render: () =>

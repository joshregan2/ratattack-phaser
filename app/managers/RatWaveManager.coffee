class RatWaveManager
	constructor:(game) ->
		@game = game
		@ratsRemaining = 0;
		@waveRats = 0
		@currentWave = {}
		@waveComplete = false
		@waveStarted = false

	prepareWave:(waveNumber)=>
		console.log('getting wave ' + waveNumber + ' ready')
		@currentWave = new RatWave(waveNumber)
		@ratsRemaining = @currentWave.totalRats
		@waveRats = @currentWave.totalRats

	startWave:()->
		@waveStarted = true

	spawnRat:(max) =>
		console.log('Spawned Rat')
		min = 32
		max = @game.world.width - 32
		randomX = Math.floor(Math.random() * (max - min + 1)) + min;

		ratTypes = _.keys(SharedGameConfig.RAT_TYPES)
		randomRatType = ratTypes[Math.floor(Math.random() * (ratTypes.length))]

		@mutantRat = new MutantRat(@game, randomX, 64, randomRatType)
	
	killRat:()->
		console.log('Killed a rat!')
		@ratsRemaining--
		console.log(@waveRats, @ratsRemaining)
		@checkWaveStatus()

	checkWaveStatus:()->
		if @ratsRemaining <= 0
			@endWave()

	endWave:()->
		@waveComplete = true